<?php

/**
 * @file
 * Contains Drupal\entity_chooser\Form\Demo
 */

namespace Drupal\entity_chooser\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormBase;

class Demo extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'entity_chooser_demo_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
/*
    $entities = entity_load_multiple_by_properties('user', array('name' => 'admin'));
    print_r($entities);
    $entities = entity_load_multiple_by_properties('user', array('name' => 'senior'));

    print_r($entities);
*/


    $table['#theme'] = 'table';
    $table['#caption'] = t('See entity_chooser_element_info() for more');
    $table['#attributes'] = array('border' => 1);
    $table['#header'] = array(
      t('Entity type'),
      t('#plugin'),
      t('valid #args'),
      t('Plugin element keys'),
      t('Universal element keys')
    );
    $selectionManager = \Drupal::service('plugin.manager.entity_chooser');
    $all_definitions = $selectionManager->getDefinitions();
    foreach ($all_definitions as $def) {
      $plugin = $selectionManager->createInstance($def['id'], array());
      $defs[$plugin->getEntityType()][$def['id']] = $def;
    }


    foreach ($defs as $entity_type => $definitions) {
      $key = key($definitions);
      $table['#rows'][$key][$entity_type]['rowspan'] = count($definitions);
      $table['#rows'][$key][$entity_type]['data'] = $entity_type;

      foreach ($definitions as $id => $definition) {
        $plugin = $selectionManager->createInstance($id, array());
        $table['#rows'][$id]['pluginname'] = $id;
        $args = $plugin->validArgs();
        if (count($args)> 5) {
          $args = array_merge(array_slice($args, 0, 5), array(t('etc...')));
        }
        $table['#rows'][$id]['args'] = implode(', ', $args);
        $table['#rows'][$id]['settings'] = $this->showSettings($plugin->getElementKeys());
      }
    }

    //on the first row, last column, with the right rowspan, put the universal element keys
    reset($all_definitions);
    $firstrow = key($all_definitions);
    $settings = array(
      'include' => t('an array of entity ids to be included regardless'),
      'exclude' => t('an array of entity ids to be excluded regardless'),
    );
    $table['#rows'][$firstrow]['universal']['data'] = $this->showSettings($settings);
    $table['#rows'][$firstrow]['universal']['rowspan'] = count($all_definitions);

    $form['#prefix'] = drupal_render($table);

    $form['#tree'] = TRUE;
    foreach ($this->demos() as $delta => $info) {
      //id must be unique - this is enough for now
      $id = @$info['type'] .'-'. @$info['plugin'];
      $description = $info['description']; unset($info['description']);
      $widget = array();
      foreach ($info as $key => $val) {
        $widget['#'.$key] = $val;
      }

      $form[$delta] = array(
      	'#title' => $info['title'],
        '#description' => $description,
        '#type' => 'details',
        'code' => array(
          '#markup' => '<pre>'.print_r($widget, 1).'</pre>',
          '#weight' => 1
        ),
        "widget" => $widget,
      );
      //adjustments to the widget that don't need to be seen here
      $form[$delta]["widget"]['#weight'] = 5;
      $form[$delta]['widget']['#empty_option'] = t('- None -');
    }
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
      '#weight' => 10,
    );

    $form['#suffix'] = '* This will appear differently depending on the threshhold setting.';
    $form['#suffix'] .= '<style>div.details-wrapper{background-color:#eee; border-radius:20px;}</style>';
    return $form;

  }

  public function validateForm(array &$form, array &$form_state) {
    //not sure this is necessary
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    foreach ($this->demos() as $delta => $demo) {
      $results[$demo['description']] = $form_state['values'][$delta]['widget'];
    }

    $message = '<pre>'.print_r($results, 1).'</pre>';
    drupal_set_message($message);
    //because dsm doesn't work
    echo "drupal_set_message doesn't seem to work here<br />";
    echo 'the results from each widget are: <br />';
    die($message);

  }

  private function showSettings(array $settings) {
    $output = '';
    foreach ($settings as $key => $description) {
      $output .= "<strong>#$key:</strong> $description<br />\n";
    }
    return $output;
  }

  private function demos() {
    return array(
      array(
        'title' => t('Choose one'),
        'description' => t("Choose one user with role 'authenticated'."),
        'type' => 'entity_chooser',
        'plugin' => 'role',
        'rids' => array(DRUPAL_AUTHENTICATED_RID),
        'required' => 0,
      ),
      array(
        'title' => t('Choose several'),
        'description' => t("Choose several users with permission to access content."),
        'type' => 'entity_chooser',
        'plugin' => 'permission',
        'perms' => array('access content'),
        'multiple' => TRUE,
        'required' => 0,
      ),
      array(
        'title' => t('Choose (everyone in) a selection'),
        'description' => t("Choose all users with a given permission"),
        'type' => 'entity_chooser_selection',
        'plugin' => 'permission',
        'required' => 0,
      ),
      array(
        'title' => t('Choose (everyone in) a selection from all plugins'),
        'description' => t("Choose a selection of users from all possible selections"),
        'type' => 'entity_chooser_selection',
        'required' => 0,
      ),
      array(
        'title' => t('Choose a filter'),
        'description' => t("Choose a block from all filters created. (This is a config entity)"),
        'type' => 'entity_chooser',
        'plugin' => 'config',
        'args' => array('filter_format'),
        'required' => 0,
        //'multiple' => TRUE,
      )
    );
  }
}
