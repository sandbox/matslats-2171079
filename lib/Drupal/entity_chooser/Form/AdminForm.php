<?php

namespace Drupal\entity_chooser\Form;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AdminForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.context.free')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'entity_chooser_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $config = $this->configFactory->get('entity_chooser.config');

    $form['threshhold'] = array(
      '#title' => t('Autocomplete threshhold'),
      '#description' => t('Below this number of items, a select box is shown, above it, an autocomplete field'),
      '#type' => 'number',
      '#size' => 3,
      '#maxlength' => 3,
      '#default_value' => $config->get('threshhold'),
      '#weight' => 1,
      '#min' => 0,
    );
    $form['limit'] = array(
      '#title' => t('Autocomplete dropdown'),
      '#title' => t('Number of suggestions to show on autocomplete fields'),
      '#type' => 'number',
      '#min' => 1,
      '#max' => 25,
      '#default_value' => $config->get('limit'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $this->configFactory->get('entity_chooser.config')
      ->set('threshhold', $form_state['values']['threshhold'])
      ->set('limit', $form_state['values']['limit'])
      ->save();
    parent::submitForm($form, $form_state);
  }
}
