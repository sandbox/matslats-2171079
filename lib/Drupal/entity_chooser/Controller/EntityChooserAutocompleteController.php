<?php

/**
 * @file
 * Contains \Drupal\entity_chooser\Controller\entityChooserAutocompleteController.
 */
namespace Drupal\entity_chooser\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\entity_chooser\EntityAutocomplete;
use Drupal\Core\Form\FormBuilder;

/**
 * Controller routines for taxonomy user routes.
 */
class entityChooserAutocompleteController implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('entity.autocomplete')
    );
  }

  /**
   * Returns response for the user autocompletion.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object containing the search string.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response containing the autocomplete suggestions for existing users.
   *
   */
  public function autocomplete(Request $request) {
    extract($_GET);
    $matches = array();
    //there is probably a more Drupalish way to get $form_id, $parents, and $q from the url
    //the parents were separated by commas for the urlencoding
    $element = $this->retrieveElement($build_id, drupal_explode_tags($parents));

    $labels = drupal_explode_tags($q);
    $fragment = trim(array_pop($labels));
    $prefix = implode(', ', $labels);
    if ($prefix) $prefix .= ', ';
    entity_chooser_append_class($element);

    $readable_key = reset($element['#class']->matchAgainst());
    foreach ($element['#class']->getIdsFromString($fragment) as $id) {
      $entity = entity_load($element['#class']->getEntityType(), $id);
      $matches[] = array(
        //we get this value depending on whether it is a config entity
        'value' => method_exists($entity, 'enable') ? $entity->id() : $entity->{$readable_key}->value,
        'label' => $entity->label()
      );
    }

    return new JsonResponse($matches);
  }

  private function retrieveElement($form_build_id, $parents) {
    //see ajax_get_form
    $form = form_get_cache($form_build_id, \Drupal::formBuilder()->getFormStateDefaults());
    $element = array();
    if ($form) {
      $element = $form;
      foreach ($parents as $key) {
        $element = $element[$key];
      }
    }
    //so far we have the element as defined in the original code
    //get the default keys from element_info

    $element += element_info('entity_chooser');
    foreach ($element['#process'] as $function) {
      $element = $function($element);
    }
    return $element;
  }
}


