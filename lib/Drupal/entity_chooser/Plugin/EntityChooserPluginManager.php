<?php

/**
 * @file
 * Contains Drupal\entity_chooser\Plugin\EntityChooserPluginManager
 */

namespace Drupal\entity_chooser\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Plugin\DefaultPluginManager;

class EntityChooserPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, LanguageManager $language_manager) {
    parent::__construct(
      'Plugin/EntityChooser',
      $namespaces,
      'Drupal\entity_chooser\Annotation\EntityChooser'
    );
    $this->setCacheBackend($cache_backend, $language_manager, 'EntityChooser');
  }

  /*
   * return the plugins of a particular entityType
   * @param string $entity_type
   *   (optional) the machine name of an entity type to which the plugins apply
   * @return array
   *   a list of plugins if $entity_type is given, otherwise lists of all plugins, keyed by entity_type
   */
  public function getAllSelections($entity_type = '') {
    foreach ($this->getDefinitions() as $plugin_name => $definition) {
      $instance = $this->createInstance($plugin_name, array());
      if ($entity_type && $entity_type != $instance->getEntityType()) continue;
      $selection = array();
      foreach ($instance->validArgs() as $arg) {
        $selection[] = $plugin_name.': '.$arg;
      }
      $selections[$plugin_name] = drupal_map_assoc($selection);
    }
    return $selections;
  }



}
