<?php

/**
 * @file
 * Contains Drupal\entity_chooser\Plugin\EntityChooser\Permission
 */

namespace Drupal\entity_chooser\Plugin\EntityChooser;

use Drupal\entity_chooser\Plugin\EntityChooser\User;

/**
 * Permision selection
 *
 * @EntityChooser(
 *   id = "permission",
 *   label = @Translation("Select user by permission"),
 * )
 */
class Permission extends User {

  protected $perms;
  protected $allValidIds;


  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getAllSelections()
   */
  function getAllSelections() {
    $permission_names = \Drupal::moduleHandler()->invokeAll('permission');
    foreach ($permission_names as $key => $info)  {
      $perms[$key] = t('Permission: @perm', array('@perm' => strip_tags($info['title'])));
    }
    return $perms;
  }

  function validArgs() {
    //just return the first 5
    return array_keys(\Drupal::moduleHandler()->invokeAll('permission'));
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooser\UserBase::query()
   */
  function query() {
  	$query = $this->baseQuery();
  	//we have to calculate via roles, unfortunately
    $rids = array();
    foreach($this->perms as $perm) {
      $rids += array_keys(user_role_names(TRUE, $perm));
    }
    //if the authenticated role is selected we don't need to filter for anything.
    if (!in_array('authenticated', $rids)) {
      if (count($rids)) {
        $query->leftjoin('users_roles', 'ur', 'ur.uid = u.uid');
        $query->condition('ur.rid', array_unique($rids));
        $query->distinct();
      }
      //if $rids is empty that means only user 1 is allowed.
      //user 1 is in $included so the query should return empty
      $query->condition(1, 0);
    }
    return $query;
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getKeys()
   */
  function getKeys() {
    $keys = parent::getKeys();
    return $keys + array(
      'args' => t("array of permission names"),
    );
  }
  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::setArgs()
   */
  public function setArgs(array $args) {
    $this->perms = $args;
    //because user has permission to do anything.
    $this->include[] = 1;
    $this->include = array_unique($this->include);
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::isValid()
   */
  public function isValid($id) {
    return in_array($id, $this->getAllValidIds());
  }


}
