<?php

/**
 * @file
 * Contains Drupal\entity_chooser\Plugin\EntityChooser\User
 */

namespace Drupal\entity_chooser\Plugin\EntityChooser;

use Drupal\entity_chooser\Plugin\EntityChooserBase;

/**
 * Role selection
 *
 * @EntityChooser(
 *   id = "user",
 *   label = @Translation("Select all active users"),
 * )
 */
class User extends EntityChooserBase {

  protected $status;

  /**
   * {@inheritdoc}
   */
  function __construct($element, $id, $definition) {
    $this->entity_type = 'user';
    parent::__construct($element, $id, $definition);
    $this->status = array_key_exists('status', $element) ? $element['#status'] : 1;
  }

  /**
   * helper funtion for selecting on the users table
   */
  function baseQuery() {
    $query = db_select('users', 'u')
      ->fields('u', array('uid'))
      ->condition('u.uid', 0, '!=');
    if ($this->status) {
      $query->condition('u.status', 1);
    }
    return $query;
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getKeys()
   */
  public function getElementKeys() {
    return array(
      'status' => t('TRUE to select only active users')
    );
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::validArgs()
   */
  function validArgs() {
    return array(t('All authenticated users'));
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getAllValidIds()
   */
  public function getAllValidIds() {
    //using the plugin property as cache
    if (empty($this->allValidIds)) {
      $result = $this->baseQuery()->execute();
      $this->allValidIds = entity_chooser_include_exclude($result->fetchCol(), $this->include, $this->exclude);
    }
    return $this->allValidIds;
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getIdsFromString()
   */
  public function getIdsFromString($string) {
    $query = $this->query(array());

    if ($limit = \Drupal::config('entity_chooser.config')->get('limit')) {
      $query->range(0, $limit);
    }

    $string = '%'. db_like($string).'%';
    //in D7 there was a setting to decide whether to put the initial % there.
    $condition = db_or();
    foreach ($this->matchAgainst() as $fieldname) {
      $condition->condition('u.'.$fieldname, $string, 'LIKE');
    }
    $query->condition($condition);
    $result = $query->execute()->fetchCol();
    return entity_chooser_include_exclude($query->execute()->fetchCol(), $this->include, $this->exclude);
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::match_against()
   */
  public function matchAgainst() {
    return array('name', 'mail', 'uid');
  }

  public function setArgs(array $args) {
    if (count($args)) die(t("Can't send args to entity chooser '@name' plugin", array('@name' => $this->id())));
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::isValid()
   */
  public function isValid($uid) {
    return user_load($uid)->id();
  }

}
