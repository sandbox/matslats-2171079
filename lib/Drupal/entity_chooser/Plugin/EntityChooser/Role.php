<?php

/**
 * @file
 * Contains Drupal\entity_chooser\Plugin\EntityChooser\Role
 */

namespace Drupal\entity_chooser\Plugin\EntityChooser;

use Drupal\entity_chooser\Plugin\EntityChooser\User;

/**
 * Role selection
 *
 * @EntityChooser(
 *   id = "role",
 *   label = @Translation("Select user by role"),
 * )
 */
class Role extends User {

  protected $rid;
  protected $allValidIds;

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getAllSelections()
   */
  function getAllSelections() {
    foreach (user_roles(TRUE) as $key => $role) {
      $roles[$key] = $role->label();
    }
    return $roles;
  }

  function validArgs() {
    return array_keys(user_roles(TRUE));
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooser\UserBase::_query()
   */
  function query() {
  	$query = $this->baseQuery();
    //if the authenticated role is there we don't need to filter for anything.
    if ($this->rid != DRUPAL_AUTHENTICATED_RID && !empty($this->rid)) {
      $query->leftjoin('users_roles', 'ur', 'ur.uid = u.uid');
      $query->condition('ur.rid', $this->rid);
    }
    return $query;
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::setArgs()
   */
  public function setArgs(array $args) {
    $this->rid = reset($args);
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getKeys()
   */
  function getKeys() {
    $keys = parent::getElementKeys();
    return $keys + array(
      'args' => t("array of role ids"),
    );
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::isValid()
   */
  public function isValid($id) {
    //or we could do a quick db call to users_roles table
    return array_key_exists($this->rid, user_load($id)->roles);
  }

}
