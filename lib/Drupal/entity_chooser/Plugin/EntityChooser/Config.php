<?php

/**
 * @file
 * Contains Drupal\entity_chooser\Plugin\EntityChooser\Config
 */

namespace Drupal\entity_chooser\Plugin\EntityChooser;

use Drupal\entity_chooser\Plugin\EntityChooserBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Choose a configuration entity
 *
 * @EntityChooser(
 *   id = "config",
 *   label = @Translation("Select a config entity"),
 * )
 */
class Config extends EntityChooserBase {

  /**
   * {@inheritdoc}
   */
  function __construct($element, $id, $definition) {
    parent::__construct($element, $id, $definition);
    if (array_key_exists('#args', $element)) {
      if (empty($element['#args']))mtrace();
      $this->setArgs($element['#args']);
    }
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getKeys()
   */
  public function getElementKeys() {
    return array();
  }


  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::validString()
   */
  public function getIdsFromString($string) {
    $entities = entity_load_multiple($this->entity_type);

    $limit = \Drupal::config('entity_chooser.config')->get('limit');
    while (count($results) <= $limit && count($entities)) {
      $entity = array_shift($entities);
      if (strpos($entity->label(), $string)) {
        $results[] = $entity->id();
      }
    }
    return $this->entity_chooser_include_exclude($results, $this->include, $this->exclude);
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getAllValidIds()
   */
  public function getAllValidIds() {
    return entity_chooser_include_exclude(
      array_keys(entity_load_multiple($this->entity_type)),
      $this->include,
      $this->exclude
    );
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserBase::validArgs()
   */
  function validArgs() {
    return array_keys(entity_get_info());
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::isValid()
   */
  public function isValid($id) {
    return in_array($id, getAllValidIds());
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::match_against()
   */
  public function matchAgainst() {
    return array('id');
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::setArgs()
   */
  function setArgs(array $args) {
    $this->entity_type = $args[0];
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserBase::getEntityType()
   */
  function getEntityType() {
    //can we not access the plugin $definition directly from here?
    return $this->entity_type;
  }

}
