<?php

/**
 * @file
 * Contains Drupal\entity_chooser\Plugin\EntityChooser\Current
 */

namespace Drupal\entity_chooser\Plugin\EntityChooser;

use Drupal\entity_chooser\Plugin\EntityChooser\User;

/**
 * Select the current user only
 *
 * @EntityChooser(
 *   id = "currentuser",
 *   label = @Translation("The current user only"),
 * )
 */
class Current extends User {

  protected $allValidIds;

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::isValid()
   */
  public function isValid($id) {
    return $id == \Drupal::currentUser()->id();
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooser\User::getElementKeys()
   */
  function getElementKeys(){
    return array();
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooser\User::validArgs()
   */
  function validArgs() {
    return array(t('The current user only'));
  }
}
