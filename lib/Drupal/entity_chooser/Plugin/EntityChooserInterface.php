<?php

/**
 * @file
 * Contains Drupal\entity_chooser\Plugin\EntityChooserInterface.
 */

namespace Drupal\entity_chooser\Plugin;


//TODO should we extend EntityInterface or ConfigEntityInterface?
//seems to only allow public functions
interface EntityChooserInterface {


  /**
   * Documention function
   * retrieve the element_info keys that this plugin understands
   * N.B 'args' is a bit of gray area. If your plugin takes args, use this.
   * @return array
   *   key is the element_info key, without a hash, value is description
   */
  function getElementKeys();

  /**
   * generate a list with all possible arguments this plugin can accept
   * if there are no args the base plugin returns array(t('Default'))
   * @return array
   *   with each value as one valid argument
   */
  function validArgs();

  /**
   * return a complete list of valid entity ids, or only the valid ids passed in the argument
   * @param unknown $edit
   *   an entity_id or array of entity_ids to test
   * @return array
   *   the valid ids
   */
  public function getAllValidIds();

  /**
   * Check that an id is within the selection
   * @param mixed $id
   * @return bool
   *   TRUE if the id is valid
   */
  public function isValid($id);


  /**
   * check a string from the autocomplete widget and return the entity_id
   * @param array $string
   *   The fragment to search on
   * @param integer $limit
   *   the maximum number of results
   * @return array
   *   all the ids which match the string
   */
  public function getIdsFromString($string);

  /**
   * get the fields in the entity db table to match against
   * the first one must be as similar to the entity label as possible.
   * the autocomplete field needs a value to show after the selection is made
   * when autocomplete is submitted, it then looks up the value again, using this key
   * for user entity 'name' is the best key, because it is usually identical to the $entity->label()
   * @return array
   *   the raw fieldnames
   */
  public function matchAgainst();

  /**
   * Gets the machine name of the chooser's entity
   * @return string
   *   the entityType
   */
  public function getEntityType();

  /**
   * at the time of writing there is only ever one argument.
   * Note that it is used differently for config and config entities.
   * @param array $args
   */
  function setArgs(array $args);

}
