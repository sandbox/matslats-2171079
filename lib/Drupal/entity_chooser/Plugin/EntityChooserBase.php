<?php

/**
 * @file
 *  Contains Drupal\entity_chooser\Plugin\EntityChooserBase.
 */
namespace Drupal\entity_chooser\Plugin;

use Drupal\entity_chooser\Plugin\EntityChooserInterface;
use Drupal\Core\Database\Query\Condition;

/**
 * Base class for selecting regardless of entity.
 */
abstract class EntityChooserBase implements EntityChooserInterface {

  /**
   * The entity type upon which this plugin operations
   */
  protected $entity_type;

  /**
   * The #excluded entity_ids as passed in the FormAPI element
   */
  protected $exclude;

  /**
   * The #included entity_ids as passed in the FormAPI element
   */
  protected $include;


  /**
   * Set any properties of this plugin from the given $element
   *
   * @param array $element
   *   may be empty
   * @param string $id
   *   the plugin id
   * @param array $definition
   *   the plugin definition
   */
  function __construct($element, $id, $definition) {
    if ($element) {
      $this->exclude = $element['#exclude'];
      $this->include = $element['#include'];
      if (array_key_exists('#args', $element)) {
        $this->setArgs($element['#args']);
      }
    }
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getEntityType()
   */
  function getEntityType() {
    //can we not access the plugin $definition directly from here?
    return $this->entity_type;
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::validArgs()
   */
  function validArgs() {
    return array(t('Default'));
  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::setArgs()
   */
  function setArgs(array $args) {

  }

  /**
   * @see \Drupal\entity_chooser\Plugin\EntityChooserInterface::getKeys()
   */
  public function getElementKeys() {
    return array();
  }

}

