<?php

/**
 * @file
 * Contains \Drupal\entity_chooser\Annotation\EntityChooser
 */

namespace Drupal\entity_chooser\Annotation;

use Drupal\Core\TypedData\Annotation\DataType;

/**
 * Defines an EntityChooser annotation object.
 *
 * @Annotation
 */
class EntityChooser extends DataType {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The default human-readable name of the plugin
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}
